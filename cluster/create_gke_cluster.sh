#!/bin/sh

projectID=reference-unity-314207

gcloud beta container --project "$projectID" clusters create "mvp-otus-kz" \
                      --zone "us-central1-c" \
                      --no-enable-basic-auth \
                      --cluster-version "1.19.9-gke.1900" \
                      --release-channel "regular" --machine-type "n1-standard-2" \
                      --image-type "COS_CONTAINERD" \
                      --disk-type "pd-standard" \
                      --disk-size "100" \
                      --metadata disable-legacy-endpoints=true \
                      --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
                      --max-pods-per-node "110" \
                      --num-nodes "4" \
                      --enable-stackdriver-kubernetes \
                      --enable-ip-alias \
                      --network "projects/$projectID/global/networks/default" \
                      --subnetwork "projects/$projectID/regions/us-central1/subnetworks/default" \
                      --no-enable-intra-node-visibility \
                      --default-max-pods-per-node "110" \
                      --no-enable-master-authorized-networks \
                      --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver \
                      --enable-autoupgrade \
                      --enable-autorepair \
                      --max-surge-upgrade 1 \
                      --max-unavailable-upgrade 0 \
                      --enable-shielded-nodes \
                      --node-locations "us-central1-c"
